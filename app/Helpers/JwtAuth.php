<?php

namespace App\Helpers;

use App\Models\User;
use DomainException;
use Firebase\JWT\JWT;
use UnexpectedValueException;

class JwtAuth{

    public $key;

    public function __construct()
    {
        $this->key = 'alboad1xto';
    }

    public function singUp($email, $password, $getToken = null){

        //* BUSCAR SI EXISTE EL USUARIO
        $user = User::where([
            'email'     => $email,
            'password'  => $password
        ])->first();

        //* COMPROBAR SI LOS DATOS SON CORRECTOS (OBJETOS)
        $singup = false;
        if(is_object($user)){
            $singup = true;
        }

        //* GENERAR EL TOKEN CON LOS DATOS DEL USUARIO IDENTIFICADO
        if(!$singup){

            $data = array(
                'status'    => 'error',
                'message'   => 'Revisa tu e-mail o contraseña.'
            );

        }else{

            //* BUSCAR IMAGEN DEL USUARIO
            $user_image = $user->image;

            $token = array(
                'sub'   => $user->id,
                'email' => $user->email,
                'name'  => $user->name,
                'iat'   => time(), //cuando se ha creado el token
                'exp'   => time() + (7 * 24 * 60 * 60), //cuando va a expirar el token (1 semana de duración)
                'img'   => $user_image ? $user_image->url : ''
            );

            $jwt        = JWT::encode($token, $this->key, 'HS256');
            $decoded    = JWT::decode($jwt, $this->key, ['HS256']);

            //* DEVOLVER LOS DATOS DECODIFICADOS DEL USUARIO O EL TOKEN, EN FUNCION DE UN PARAMETRO
            if(is_null($getToken)){
                $data = $jwt;
            }else{
                $data = $decoded;
            }

        }

        return $data;

    }

    public function checkToken($jwt, $getIdentity = false){

        $auth = false;

        try{

            $jwt        = str_replace('"', '', $jwt);
            $decoded    = JWT::decode($jwt, $this->key, ['HS256']);

        }catch(UnexpectedValueException $e){

            $auth = false;

        }catch(DomainException $e){

            $auth = false;

        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub)){

            $auth = true;

        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;

    }

}
