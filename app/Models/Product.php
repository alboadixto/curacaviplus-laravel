<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    //* RELACION UNO A MUCHOS (INVERSA)
    public function Company(){
        return $this->belongsTo('App\Models\Company');
    }

    //* RELACION UNO A UNO POLIMORFICA
    public function image(){
        return $this->morphOne('App\Models\Image', 'imageable');
    }
}
