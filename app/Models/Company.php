<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $guarded = ['id'];


    //* RELACION UNO A UNO POLIMORFICA
    public function image(){
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    //* RELACION UNO A MUCHOS (INVERSA)
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    //* RELACION UNO A MUCHOS
    public function products(){
        return $this->hasMany('App\Models\Product');
    }

    //* RELACION UNO A MUCHOS
    public function address(){
        return $this->hasMany('App\Models\Address');
    }
}
