<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Models\Address;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\VarDumper;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['index', 'show', 'getImage']]);
    }

    public function index(Request $request){

        //* COMPROBAR SI EL USUARIO ESTA IDENTIFICADO
        $token      = $request->header('Authorization');
        $jwtAuth    = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);

        if(!$checkToken){

            //* LA VALIDACIÓN HA FALLADO
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Por favor vuelva a iniciar sesión.'
            );

        }else{

            //* SACAR USUARIO IDENTIFICADO
            $user = $jwtAuth->checkToken($token, true);

            $companies = Company::where('user_id', $user->sub)->with(['image', 'address', 'products.image'])->get();

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'companies' => $companies,
            );

        }

        return response()->json($data, $data['code']);

    }

    public function show($id){

        $company = Company::find($id);

        if(!is_object($company)){

            $data = [
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'La empresa no existe'
            ];

        }else{

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'company'   => $company
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function store(Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(empty($param_array)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* VALIDAR DATOS
            $attributes = [
                'name'          => 'nombre',
                'description'   => 'descripcion',
                'image'         => 'imagen',
                'calle'         => 'calle o pasaje',
                'numero'        => 'numero de calle o pasaje',
                'phone'         => 'teléfono',
                'horario_ini'   => 'horario apertura',
                'horario_fin'   => 'horario cierre',
                'valor_envio'   => 'valor de envio',

            ];

            $validator      = Validator::make($param_array, [
                'name'          => 'required|max:45',
                'description'   => 'max:2000',
                'image'         => 'required',
                'calle'         => 'required|max:45',
                'numero'        => 'required|numeric',
                'villa'         => 'max:45',
                'referencia'    => 'max:45',
                'phone'         => 'required|min:9|max:9',
                'horario_ini'   => 'required|date_format:H:i',
                'horario_fin'   => 'required|date_format:H:i|after:horario_ini',
                'valor_envio'   => 'required|integer',
            ], [], $attributes);

            if($validator->fails()){

                $data = [
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Error al guardar la empresa.',
                    'errors'    => $validator->errors()
                ];

            }else{

                //* USUARIO IDENTIFICADO
                $user       = $request['user'];

                //* CREAR COMPANY
                $company = Company::create([
                    'name'          => $param_array['name'],
                    'description'   => empty($param_array['description']) ? null : $param_array['description'],
                    'user_id'       => $user->sub
                ]);

                //* GUARDAR IMAGEN EN BD
                $company->image()->create(['url' => $param_array['image']]);

                //* CREAR ADDRESS
                $company->address()->create([
                    'calle'       => $param_array['calle'],
                    'numero'      => $param_array['numero'],
                    'villa'       => empty($param_array['villa']) ? null : $param_array['villa'],
                    'referencia'  => empty($param_array['referencia']) ? null : $param_array['referencia'],
                    'phone'       => $param_array['phone'],
                    'horario_ini' => $param_array['horario_ini'],
                    'horario_fin' => $param_array['horario_fin'],
                    'valor_envio' => $param_array['valor_envio'],
                ]);

                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'El negocio se ha creado correctamente.',
                    'company'   => $company
                );

            }

        }

        return response()->json($data, $data['code']);

    }

    public function update($id, Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(empty($param_array)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* VALIDAR DATOS
            $attributes = [
                'name'          => 'nombre',
                'description'   => 'descripcion',
                'image'         => 'imagen',
                'calle'         => 'calle o pasaje',
                'numero'        => 'numero de calle o pasaje',
                'phone'         => 'teléfono',
                'horario_ini'   => 'horario apertura',
                'horario_fin'   => 'horario cierre',
                'valor_envio'   => 'valor de envio',

            ];

            $validator      = Validator::make($param_array, [
                'name'          => 'required|max:45',
                'description'   => 'max:2000',
                'image'         => 'required',
                'calle'         => 'required|max:45',
                'numero'        => 'required|numeric',
                'villa'         => 'max:45',
                'referencia'    => 'max:45',
                'phone'         => 'required|min:9|max:9',
                'horario_ini'   => 'required',
                'horario_fin'   => 'required|after:horario_ini',
                'valor_envio'   => 'required|integer',
            ], [], $attributes);

            if($validator->fails()){

                $data = [
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Error al actualizar el negocio.',
                    'errors'    => $validator->errors()
                ];

            }else{

                //* BUSCAR LA EMPRESA QUE SE SUBIRA LA FOTO DE PERFIL
                $image       = Company::find($id);
                //* SE DEBE ELIMINAR LA FOTO ANTES DE SUBIR UNA NUEVA YA QUE SOLO PERMITE 1 FOTO POR EMPRESA EN BD
                $image->image()->delete();
                //* GUARDAR IMAGEN EN BD
                $image->image()->create(['url' => $param_array['image']]);

                //* ACTUALIZAR COMPANY
                Company::where('id', $id)->update([
                    'name' => $param_array['name'],
                    'description' => $param_array['description']
                ]);

                //* ACTUALIZAR ADDRESS
                Address::where('company_id', $id)->update([
                    'calle'       => $param_array['calle'],
                    'numero'      => $param_array['numero'],
                    'villa'       => empty($param_array['villa']) ? null : $param_array['villa'],
                    'referencia'  => empty($param_array['referencia']) ? null : $param_array['referencia'],
                    'phone'       => $param_array['phone'],
                    'horario_ini' => $param_array['horario_ini'],
                    'horario_fin' => $param_array['horario_fin'],
                    'valor_envio' => $param_array['valor_envio'],
                ]);

                //* DEVOLVER ARRAY CON RESULTADOS
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'El negocio se ha actualizado.',
                    'change'    => $param_array
                );

            }

        }

        return response()->json($data, $data['code']);

    }

    public function upload(Request $request){

        //* RECOGER DATOS DE LA PETICIÓN
        $image = $request->file('file');

        //* VALIDAR IMAGEN
        $validator      = Validator::make($request->all(), [
            'file'      => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        if(!$image || $validator->fails()){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error al subir imagen',
                'errors'    => $validator->errors()
            );

        }else{

            //* GUARDAR IMAGEN
            $image_name = time().$image->getClientOriginalName();

            //* GUARDAR IMAGEN COMPRIMIDA
            $ruta       = storage_path() . '/app/images/companies/' . $image_name;
            Image::make($image)
            ->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            })
            ->save($ruta);

            //* GUARDAR IMAGEN SIN COMPRIMIR
            //Storage::disk('companies')->put($image_name, File::get($image));

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'image'      => $image_name
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function getImage($filename){

        $isset = Storage::disk('companies')->exists($filename);

        if(!$isset){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'user'      => 'La imagen no existe'
            );

            return response()->json($data, $data['code']);

        }else{

            $file = Storage::disk('companies')->get($filename);

            return new Response($file, 200);

        }

    }

    public function destroy($id){

        //* CONSEGUIR EL REGISTRO
        $company = Company::find($id);

        if(empty($company)){

            $data = [
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'El negocio no existe.'
            ];

        }else{

            //* ELIMINO EL NEGOCIO
            $company->delete();

            //* ELIMINO LA IMAGEN
            $company->image()->delete();

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'El negocio ha sido eliminado.',
                'company'   => $company
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function is_active($id, Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json     = $request->input('json', null);
        $value    = json_decode($json, true);

        if(($value != 0 && $value != 1)  || empty($id)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* ACTUALIZAR EMPRESA EN BD
            Company::where('id', $id)->update([
                'is_active'     => $value
            ]);

            //* DEVOLVER ARRAY CON RESULTADOS
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'El negocio ha cambiado de estado.'
            );

        }

        return response()->json($data, $data['code']);

    }
}
