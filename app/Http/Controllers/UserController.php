<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    public function register(Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(empty($param_array)){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Los datos enviados no son correctos'
            );

        }else{

            //* LIMPIAR DATOS
            $param_array    = array_map('trim', $param_array);

            //* VALIDAR DATOS
            $validator      = Validator::make($param_array, [
                                'name'      => 'required|regex:/^[\pL\s\-]+$/u|max:45',
                                'email'     => 'required|email|unique:users|max:45',
                                'password'  => 'required|max:255'
            ]);

            if($validator->fails()){

                $data = array(
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'El usuario no se ha creado',
                    'errors'    => $validator->errors()
                );

            }else{

                //* CIFRAR LA CONTRASEÑA
                $param_array['password'] = hash('sha256',$param_array['password']);

                //* CREAR USUARIO
                $user = User::create([
                    'name'      => $param_array['name'],
                    'email'     => $param_array['email'],
                    'password'  => $param_array['password']
                ]);

                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'El usuario se ha creado correctamente.',
                    'user'      => $user
                );

            }

        }

        return response()->json($data, $data['code']);

    }

    public function login(Request $request){

        $jwtAuth = new JwtAuth();

        //* RECIBIR DATOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        //* VALIDAR DATOS
        $validator      = Validator::make($param_array, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if($validator->fails()){
            //* LA VALIDACIÓN HA FALLADO
            $singup = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'El usuario no se ha podido identificar',
                'errors'    => $validator->errors()
            );

        }else{
            //* CIFRAR LA CONTRASEÑA
            $param_array['password'] = hash('sha256',$param_array['password']);

            //* DEVOLVER TOKEN O DATOS
            $singup = $jwtAuth->singUp($param_array['email'], $param_array['password']);

            if(!empty($param_array['getToken'])){
                $singup = $jwtAuth->singUp($param_array['email'], $param_array['password'], true);
            }

        }

        return response()->json($singup, 200);

    }

    public function update(Request $request){

        //* COMPROBAR SI EL USUARIO ESTA IDENTIFICADO
        $token      = $request->header('Authorization');
        $jwtAuth    = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(!$checkToken || empty($param_array)){

            //* LA VALIDACIÓN HA FALLADO
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'El usuario no esta identificado.'
            );

        }else{

            //* SACAR USUARIO IDENTIFICADO
            $user = $jwtAuth->checkToken($token, true);

            //* CIFRAR LA CONTRASEÑA ACTUAL
            $param_array['currentPassword'] = hash('sha256',$param_array['currentPassword']);

            //* BUSCAR SI EL USUARIO TIENE LA CONTRASEÑA CORRECTA
            $pass = User::where([
                'id'        => $user->sub,
                'password'  => $param_array['currentPassword']
            ])->first();

            if(!$pass){

                //* LA VALIDACIÓN HA FALLADO
                $data = array(
                    'status'    => 'errorPass',
                    'code'      => 404,
                    'message'   => 'La contraseña es incorrecta.'
                );

            }else{

                //* VALIDAR DATOS
                $validator      = Validator::make($param_array, [
                    'name'      => 'required|regex:/^[\pL\s\-]+$/u|max:45',
                    'email'     => 'required|email|max:45|unique:users,email,'.$user->sub
                ]);

                if($validator->fails()){

                    $data = [
                        'status'    => 'error',
                        'code'      => 404,
                        'message'   => 'Error al actualizar',
                        'errors'    => $validator->errors()
                    ];

                }else{

                    //* BUSCAR AL USUARIO QUE SE SUBIRA LA FOTO DE PERFIL (AVATAR)
                    $user_image       = User::find($user->sub);
                    //* SE DEBE ELIMINAR LA FOTO ANTES DE SUBIR UNA NUEVA YA QUE SOLO PERMITE 1 FOTO POR USUARIO EN BD
                    $user_image->image()->delete();
                    //* GUARDAR AVATAR EN BD
                    $user_image->image()->create(['url' => $param_array['img']]);

                    //* QUITAR LOS CAMPOS QUE NO QUIERO ACTUALIZAR
                    unset($param_array['id']);
                    unset($param_array['role']);
                    unset($param_array['checkPassword']);
                    unset($param_array['currentPassword']);
                    unset($param_array['created_at']);
                    unset($param_array['remember_token']);
                    unset($param_array['img']);

                    //* VERIFICAR QUE VIENE VALOR EN EL CAMBIO DE CONTRASEÑA
                    if($param_array['password'] == ""){
                        unset($param_array['password']);
                    }else{
                        //* CIFRAR LA CONTRASEÑA
                        $param_array['password'] = hash('sha256',$param_array['password']);
                    }

                    //* ACTUALIZAR USUARIO EN BD
                    User::where('id', $user->sub)->update($param_array);

                    //* DEVOLVER ARRAY CON RESULTADOS
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'El usuario se ha actualizado.',
                        'user'      => $user,
                        'change'    => $param_array
                    );

                }
            }

        }

        return response()->json($data, $data['code']);

    }

    public function upload(Request $request){

        //* RECOGER DATOS DE LA PETICIÓN
        $image = $request->file('file');

        //* VALIDAR IMAGEN
        $validator      = Validator::make($request->all(), [
            'file'      => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        if(!$image || $validator->fails()){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Error al subir imagen',
                'errors'    => $validator->errors()
            );

        }else{

            //* GUARDAR IMAGEN
            $image_name = time().$image->getClientOriginalName();

            //* GUARDAR IMAGEN COMPRIMIDA
            $ruta       = storage_path() . '/app/images/users/' . $image_name;
            Image::make($image)
            ->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save($ruta);

            //* GUARDAR IMAGEN SIN COMPRIMIR
            //Storage::disk('users')->put($image_name, File::get($image));

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'image'     => $image_name
            );

        }

        return response()->json($data, $data['code']);

    }

    public function getImage($filename){

        $isset = Storage::disk('users')->exists($filename);

        if(!$isset){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'user'      => 'La imagen no existe'
            );

            return response()->json($data, $data['code']);

        }else{

            $file = Storage::disk('users')->get($filename);

            return new Response($file, 200);

        }

    }

    public function detail($id){

        $user = User::find($id);

        if(!is_object($user)){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'El usuario no existe'
            );

        }else{

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'user'      => $user
            );

        }

        return response()->json($data, $data['code']);

    }

    public function forgotPassword(Request $request){
        //* RECIBIR DATOS POR POST
        $json                   = $request->input('json', null);
        $param_array['email']   = json_decode($json, true);

        //* VALIDAR DATOS
        $validator      = Validator::make($param_array, [
            'email'          => 'exists:users|required',
        ]);

        if($validator->fails()){
            //* LA VALIDACIÓN HA FALLADO
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'errors'    => $validator->errors()
            );

        }else{

            $status = Password::sendResetLink(
                $param_array
            );

            $status === Password::RESET_LINK_SENT
                        ? back()->with(['status' => __($status)])
                        : back()->withErrors(['email' => __($status)]);

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Email enviado a '.$param_array['email']
            );

        }

        return response()->json($data, $data['code']);

    }

    public function reset() {
        $credentials = request()->validate([
            'email'     => 'exists:users|required',
            'token'     => 'required|string',
            'password'  => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {

            //* CIFRAR LA CONTRASEÑA
            $param_array['password'] = hash('sha256', $password);

            $user->password = $param_array['password'];
            $user->save();

        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            //return response()->json(["msg" => "Invalid token provided"], 400);
            return view('auth.passwords.reset')->with('errorMsg','Ha ocurrido un error');
        }

        //return response()->json(["msg" => "Password has been successfully changed"]);
        return view('auth.passwords.reset')->with('successMsg','La contraseña se ha cambiado correctamente');


    }
}
