<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['index', 'show', 'getImage', 'getProductsByCompany']]);
    }

    public function index(){

        $products = Product::all();

        return response()->json([
            'status'    => 'success',
            'code'      => 200,
            'products' => $products,
        ]);

    }

    public function show($id){

        $product = Product::find($id);

        if(!is_object($product)){

            $data = [
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'El producto no existe'
            ];

        }else{

            $product->load('company');

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'product'   => $product
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function store(Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(empty($param_array)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* VALIDAR DATOS
            $attributes = [
                'name'          => 'nombre',
                'description'   => 'descripcion',
                'image'         => 'imagen',
                'price'         => 'precio',
                'stock'         => 'cantidad',
                'company_id'    => 'empresa id'
            ];

            $validator      = Validator::make($param_array, [
                'name'          => 'required|max:45',
                'description'   => 'max:2000',
                'image'         => 'required',
                'price'         => 'required|numeric',
                'stock'         => 'numeric',
                'company_id'    => 'required'
            ], [], $attributes);

            if($validator->fails()){

                $data = [
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Error al guardar el producto.',
                    'errors'    => $validator->errors()
                ];

            }else{

                //* CREAR PRODUCT
                $product = Product::create([
                    'name'          => $param_array['name'],
                    'description'   => $param_array['description'] ? $param_array['description'] : null,
                    'price'         => $param_array['price'],
                    'stock'         => $param_array['stock'] ? $param_array['stock'] : null,
                    'company_id'    => $param_array['company_id'],
                ]);

                //* GUARDAR IMAGEN EN BD
                $product->image()->create(['url' => $param_array['image']]);

                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'El producto se ha creado correctamente.',
                    'product'   => $product
                );

            }

        }

        return response()->json($data, $data['code']);

    }

    public function update($id, Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json           = $request->input('json', null);
        $param_array    = json_decode($json, true);

        if(empty($param_array)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* VALIDAR DATOS
            $attributes = [
                'name'          => 'nombre',
                'description'   => 'descripcion',
                'price'         => 'precio'
            ];

            $validator      = Validator::make($param_array, [
                'name'          => 'required|max:45',
                'description'   => 'max:2000',
                'price'         => 'required|numeric'
            ], [], $attributes);

            if($validator->fails()){

                $data = [
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Error al actualizar el producto',
                    'errors'    => $validator->errors()
                ];

            }else{

                //* BUSCAR EL PRODUCTO QUE SE SUBIRA LA FOTO DE PERFIL
                $image       = Product::find($id);
                //* SE DEBE ELIMINAR LA FOTO ANTES DE SUBIR UNA NUEVA YA QUE SOLO PERMITE 1 FOTO POR EMPRESA EN BD
                $image->image()->delete();
                //* GUARDAR IMAGEN EN BD
                $image->image()->create(['url' => $param_array['image']]);

                //* ACTUALIZAR PRODUCTO EN BD
                Product::where('id', $id)->update([
                    'name'          => $param_array['name'],
                    'description'   => $param_array['description'] ? $param_array['description'] : null,
                    'price'         => $param_array['price'],
                    'stock'         => $param_array['stock'] != "" ? $param_array['stock'] : null
                ]);

                //* DEVOLVER ARRAY CON RESULTADOS
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'El producto se ha actualizado.',
                    'change'    => $param_array
                );

            }

        }

        return response()->json($data, $data['code']);

    }

    public function destroy($id){

        //* CONSEGUIR EL REGISTRO
        $product = Product::find($id);

        if(empty($product)){

            $data = [
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'El producto no existe.'
            ];

        }else{

            //* ELIMINO EL PRODUCTO
            $product->delete();

            //* ELIMINO LA IMAGEN
            $product->image()->delete();

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'El producto ha sido eliminado.',
                'product'   => $product
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function upload(Request $request){

        //* RECOGER DATOS DE LA PETICIÓN
        $image = $request->file('file');

        //* VALIDAR IMAGEN
        $validator      = Validator::make($request->all(), [
            'file'      => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        if(!$image || $validator->fails()){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error al subir imagen',
                'errors'    => $validator->errors()
            );

        }else{

            //* GUARDAR IMAGEN
            $image_name = time().$image->getClientOriginalName();

            //* GUARDAR IMAGEN COMPRIMIDA
            $ruta       = storage_path() . '/app/images/products/' . $image_name;
            Image::make($image)
            ->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                //$constraint->upsize();
            })
            ->save($ruta);

            //* GUARDAR IMAGEN SIN COMPRIMIR
            //Storage::disk('products')->put($image_name, File::get($image));

            $data = [
                'status'    => 'success',
                'code'      => 200,
                'image'      => $image_name
            ];

        }

        return response()->json($data, $data['code']);

    }

    public function getImage($filename){

        $isset = Storage::disk('products')->exists($filename);

        if(!$isset){

            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'user'      => 'La imagen no existe'
            );

            return response()->json($data, $data['code']);

        }else{

            $file = Storage::disk('products')->get($filename);

            return new Response($file, 200);

        }

    }

    public function getProductsByCompany($id){

        $products = Product::where('company_id', $id)->get();

        $data = array(
            'status'    => 'success',
            'code'      => 200,
            'products'  => $products
        );

        return response()->json($data, $data['code']);
    }

    public function is_active($id, Request $request){

        //* RECOGER LOS DATOS ENVIADOS POR POST
        $json     = $request->input('json', null);
        $value    = json_decode($json, true);

        if(($value != 0 && $value != 1)  || empty($id)){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Los datos enviados no son correctos.'
            );

        }else{

            //* ACTUALIZAR EMPRESA EN BD
            Product::where('id', $id)->update([
                'is_active'     => $value
            ]);

            //* DEVOLVER ARRAY CON RESULTADOS
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'El producto ha cambiado de estado.'
            );

        }

        return response()->json($data, $data['code']);

    }
}
