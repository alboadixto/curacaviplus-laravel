<?php

namespace App\Http\Middleware;

use App\Helpers\JwtAuth;
use Closure;
use Illuminate\Http\Request;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        //* COMPROBAR SI EL USUARIO ESTA IDENTIFICADO
        $token      = $request->header('Authorization');
        $jwtAuth    = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);

        if(!$checkToken){

            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'El usuario no esta identificado.'
            );

            return response()->json($data, $data['code']);

        }else{

            //* SACAR USUARIO IDENTIFICADO
            $request['user'] = $jwtAuth->checkToken($token, true);

            return $next($request);
        }

    }
}
