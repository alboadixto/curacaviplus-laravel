@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>
                @php
                    $disabled = ''
                @endphp
                @if(!empty($successMsg))
                @php
                    $disabled = 'disabled'
                @endphp
                  <div class="alert alert-success alert-dismissible fade show m-3" role="alert">
                    {{ $successMsg }}<a href="http://localhost:4200/#/login">, vuelva a iniciar sesión aquí.</a>
                  </div>
                @elseif(!empty($errorMsg))
                @php
                    $disabled = 'disabled'
                @endphp
                <div class="alert alert-danger alert-dismissible fade show m-3" role="alert">
                    <strong>{{$errorMsg}}</strong><a href="http://localhost:4200/#/login">, vuelva a solicitar un nuevo cambio de contraseña aquí.</a>
                  </div>
                @endif

                <div class="card-body">
                    <form method="POST" action="api/password/reset">
                        @csrf

                        <input type="hidden" name="token" value="{{request()->get('token')}}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{request()->get('email')}}" required autocomplete="email" readonly>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" minlength="4">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" minlength="4">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" {{$disabled}}>
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
