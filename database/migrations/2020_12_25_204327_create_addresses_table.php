<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();

            $table->string('calle', 45);
            $table->unsignedInteger('numero');
            $table->string('villa', 45)->nullable();
            $table->string('referencia', 45)->nullable();
            $table->string('phone',12);
            $table->time('horario_ini');
            $table->time('horario_fin');
            $table->unsignedInteger('valor_envio')->default(0);
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('company_id');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
