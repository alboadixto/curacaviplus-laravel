<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\ApiAuthMiddleware;
use Illuminate\Support\Facades\Route;

Route::view('login', 'auth.login')->name('login');

/* USERS */
Route::post('/api/register', [UserController::class, 'register']);
Route::post('/api/login', [UserController::class, 'login']);
Route::put('/api/user/update', [UserController::class, 'update']);
Route::post('/api/user/upload', [UserController::class, 'upload'])->middleware(ApiAuthMiddleware::class);
Route::get('/api/user/image/{filename}', [UserController::class, 'getImage']);
Route::get('/api/user/detail/{id}', [UserController::class, 'detail']);

/* COMPANIES */
Route::resource('/api/company', CompanyController::class);
Route::post('/api/company/upload', [CompanyController::class, 'upload']);
Route::get('/api/company/image/{filename}', [CompanyController::class, 'getImage']);
Route::put('/api/company/is_active/{company}', [CompanyController::class, 'is_active']);

/* PRODUCTS */
Route::resource('/api/product', ProductController::class);
Route::post('/api/product/upload', [ProductController::class, 'upload']);
Route::get('/api/product/image/{filename}', [ProductController::class, 'getImage']);
Route::get('/api/product/company/{id}', [ProductController::class, 'getProductsByCompany']);
Route::put('/api/product/is_active/{product}', [ProductController::class, 'is_active']);

/* RESET PASSWORD */
Route::post('/api/forgot-password', [UserController::class, 'forgotPassword']);
Route::view('forgot_password', 'auth.passwords.reset')->name('password.reset');
Route::post('/api/password/reset', [UserController::class, 'reset']);
